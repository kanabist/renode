Connection = function(configObj, socketSettings)
{
    defaultConfig = {"port":6379, "host": "localhost"};
    this.net = require("net");
    /** Using default config if there is no configObj **/
    this.configObj = (!configObj) ? defaultConfig : {port: (configObj.port) ? configObj.port : defaultConfig.port, host: (configObj.host) ? configObj.host : defaultConfig.host};
    /** Using default socket settings if there is no configObj **/
    this.socketSettings = (!socketSettings) ? {allowHalfOpen: false, readable: false, writable: false} : socketSettings;
    this.socket = this._socketCreate(this.socketSettings);
    this._connect(this.socket, this.configObj);
}

Connection.prototype.getSocket = function()
{
    if (this.socket) {
        /* If Socket is established*/
        return this.socket;
    } else {
        throw new Error("Unable to create net socket!");
    }
}

Connection.prototype.getConfigInfo = function()
{
    if(this.configObj) {return this.configObj}
}

Connection.prototype.setConfigInfo = function(configObj)
{
    this.configObj = configObj;
}

Connection.prototype._socketCreate = function(socketSettings)
{
    return new this.net.Socket(socketSettings);
}

Connection.prototype._connect = function(socket, params)
{
    socket.connect(params.port, params.host);
    socket.on("error", function(err) {console.log(err)});
    /* Socket ondata established in Query file*/
    socket.on("connect", function(err) {if(err) {console.log("Error occured, when socket connection established!!!")}});
}

exports.$ = Connection;