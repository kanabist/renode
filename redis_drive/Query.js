events = require("events");
emitter =  require("./emitter").$.getEmitter();

/*
query.command
query.key,
query.value,
query.arguments,
 */

Query = function(redis)
{
    this.query = [];
    this.block = 0;
    this.counter = 0;
    this.redis = redis;
    self = this;
    emitter.addListener("socketWrite", this.socketWriteListener.bind(this));
    this.redis.socket.on("data", function(data) {
        self.block = 0;
        /* Proccess other query if exist*/
        console.log(data.toString());
        emitter.emit("socketWrite");
    })
}

Query.prototype.socketWriteListener = function()
{
    /*  If we have no redis query to write  */
    if(!this.block && this.query[0]) {
        this.redis.write(this.query[0]);
        this.query.shift();
        this.block = 1;
        /* We have query, and no respone! To process other queries we must get response for this first */
    }
}

Query.prototype.push = function(query)
{
    this.query.push(query);
    emitter.emit("socketWrite");
}

exports.$ = Query;
