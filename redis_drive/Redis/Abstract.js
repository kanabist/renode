var Redis_Abstract = function(socket)
{
    Query =  require("../query").$;
    this.socket = socket;
    this.query  = new Query(this);
    this.enter = "\r\n";
    inF = require("./interface").$;
    this.interface = new inF();
}

Redis_Abstract.prototype._prepareRequest = function(queue)
{
 // to say to check_value what command is?
    if(queue.command && queue.key) {
        this.interface.command = queue.command;
        request = "";
        try {
            for(var key in queue) {
                request+=this.interface["check_" + key](queue[key]);
            }
        } catch(Error)
        {
            console.log(Error);
            return this.enter;
        }
    } else {
        request =  queue;
    }
    return request + this.enter;
}

Redis_Abstract.prototype.write = function(queue)
{
    request = this._prepareRequest(queue);
    this.socket.write(request);
}

exports.$ = Redis_Abstract;
