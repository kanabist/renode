var Redis_Interface = function()
{
    this.allowCommands = ["set", "get"];
    /*  what value type we expect to get from line */
    this.valueInterface = {
        "set" : "string, number",
        "get" : "void",
        "hmset": "assoc",
        "hmget": "void",
        "zadd" : "array",
        "incr" : "void",
        "incrby": "number"
    };
}

Redis_Interface.prototype.check_value = function(values, command)
{
    command = (!command) ? this.command : command;
    type = this.valueInterface[command];
    switch(type) {
        case "string, number":
            if (typeof values == "string" || typeof values == "number") {
                return values + " ";
            } else {
                throw new Error("Values must be a string!");
            }
            break;
        case "number":
            if (typeof values == type)
            {
                return values + " ";
            } else {
                throw new Error("The value must be a number");
            }
            break;
        case "void":
            if(!values) {
                return " ";
            } else {
                throw new Error("Get functions havn`t values");
            }
            break;
        case "assoc":
            if (values && values instanceof Object && !values.length) {
                valueString="";
                for(var key in values) {
                    valueString+=key + " " + values[key] + " ";
                }

                return valueString;
            } else {
                throw new Error("Values must be an assoc array");
            }
            break;
        case "array":
            if(values instanceof Array) {
                valueString="";
                for(ar=0; ar<values.length; ar++) {
                    valueString+=ar + " " + values[ar] + " ";
                }
                return valueString;
            } else {
                throw new Error("Values must be an array!");
            }
    }
}

Redis_Interface.prototype.check_command = function(commandName)
{
    for(i=0; i<this.allowCommands.length; i++) {
        if (this.allowCommands[i] == commandName) {
            return commandName + " ";
        }
    }
    throw new Error("Incorrect command!");
}

Redis_Interface.prototype.check_key = function(key)
{
    if(key.match(/\S/g).length) {
        return key + " ";
    }
    throw new Error("Key must consist letter or decimal");
}

Redis_Interface.prototype.check_arguments = function(argumentsQ)
{
    /* length of the range or some flags  */
    if(argumentsQ && argumentsQ.length && argumentsQ instanceof Array) {
        argumentLine = "";
        for(var key in argumentsQ) {
            argumentLine+=argumentsQ[key] +  " ";
        }
        return argumentsQ;
    }
    return "";
}

exports.$ = Redis_Interface;