var Emitter = function()
{
}

Emitter.getEmitter = function()
{
    if(!this.emitter)
    {
        event = this.getEvent();
        this.emitter = new event.EventEmitter();
    }
        return this.emitter;
}

Emitter.getEvent = function()
{
    if(!this.events) {
        return require("events");
    } else {
        return this.events;
    }
}

exports.$ = Emitter;
